package com.pgallardo.rudochat

import android.app.Application
import com.onesignal.OneSignal
import com.pgallardo.rudochat.handler.MyNotificationOpenedHandler

class RudoChatApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .setNotificationOpenedHandler(MyNotificationOpenedHandler(this))
            .init()
    }

}