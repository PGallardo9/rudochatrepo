package com.pgallardo.rudochat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pgallardo.rudochat.databinding.ChatItemBinding
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.viewmodel.ChatListViewModel
import java.text.SimpleDateFormat
import java.util.*

class ChatListAdapter(private val chats: MutableList<Chat>, private val viewModel: ChatListViewModel) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ChatItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(chats[position])

    override fun getItemCount(): Int = chats.size

    class ViewHolder(val binding: ChatItemBinding, private val viewModel: ChatListViewModel) : RecyclerView.ViewHolder(binding.root) {

        fun bind(chat: Chat) {

            val sfd = SimpleDateFormat("HH:mm")
            val date = Date(chat.lastMessage?.timestamp!!)

            binding.chat = chat
            binding.lastMessageText = if(viewModel.currentUser?.uid == chat.lastMessage?.senderUid) "Tu: ${chat.lastMessage?.message}" else chat.lastMessage?.message
            binding.lastMessageHour = sfd.format(date)
            binding.viewmodel = viewModel
            binding.executePendingBindings()
        }

    }
}