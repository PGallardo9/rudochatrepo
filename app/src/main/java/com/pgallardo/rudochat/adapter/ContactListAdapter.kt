package com.pgallardo.rudochat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pgallardo.rudochat.databinding.ContactItemBinding
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.viewmodel.ChatListViewModel

class ContactListAdapter(private val chats: MutableList<User>, private val viewModel: ChatListViewModel) : RecyclerView.Adapter<ContactListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ContactItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(chats[position])

    override fun getItemCount(): Int = chats.size

    class ViewHolder(val binding: ContactItemBinding, private val viewModel: ChatListViewModel) : RecyclerView.ViewHolder(binding.root) {

        fun bind(user: User) {
            binding.user = user
            binding.viewmodel = viewModel
            binding.executePendingBindings()
        }

    }
}