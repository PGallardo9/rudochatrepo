package com.pgallardo.rudochat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pgallardo.rudochat.databinding.MessageItemBinding
import com.pgallardo.rudochat.model.Message
import com.pgallardo.rudochat.viewmodel.ChatViewModel
import java.text.SimpleDateFormat
import java.util.*

class MessageListAdapter(
    private val messages: MutableList<Message>,
    private val viewModel: ChatViewModel
) : RecyclerView.Adapter<MessageListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MessageItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(messages[position])

    override fun getItemCount(): Int = messages.size

    class ViewHolder(val binding: MessageItemBinding, private val viewModel: ChatViewModel) : RecyclerView.ViewHolder(
        binding.root
    ) {

        fun bind(message: Message) {

            val sfd = SimpleDateFormat("HH:mm")
            val date = Date(message.timestamp)

            binding.message = message
            binding.date = date
            binding.sdf = sfd
            binding.viewmodel = viewModel
            binding.executePendingBindings()

            binding.messageProfileImage.visibility = View.INVISIBLE
            binding.receiverMessageText.visibility = View.INVISIBLE
            binding.senderMesssageText.visibility = View.INVISIBLE
            
            if(viewModel.getCurrentUser()?.uid == message.senderUid) {
                binding.senderMesssageText.visibility = View.VISIBLE
            } else {
                binding.messageProfileImage.visibility = View.VISIBLE
                binding.receiverMessageText.visibility = View.VISIBLE
            }
        }

    }
}