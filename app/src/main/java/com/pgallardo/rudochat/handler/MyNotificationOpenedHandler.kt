package com.pgallardo.rudochat.handler

import android.content.Context
import android.content.Intent
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.view.activity.ChatActivity

class MyNotificationOpenedHandler(val context: Context) : OneSignal.NotificationOpenedHandler {
    override fun notificationOpened(result: OSNotificationOpenResult?) {
        val json = result?.notification?.payload?.additionalData


        val intent = Intent(context, ChatActivity::class.java).apply {
            putExtra("chat", Chat(json?.getString("senderUid"),json?.getString("receiverUid"),json?.getString("receiverName"),null))
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        context.startActivity(intent)
    }
}