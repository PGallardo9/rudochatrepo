package com.pgallardo.rudochat.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Chat(val senderUid: String?, val receiverUid: String?, val receiverName: String?, val lastMessage: Message?) : Parcelable