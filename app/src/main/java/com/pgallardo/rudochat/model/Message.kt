package com.pgallardo.rudochat.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Message(val senderUid: String = "", val message: String = "", val timestamp: Long = 0): Parcelable