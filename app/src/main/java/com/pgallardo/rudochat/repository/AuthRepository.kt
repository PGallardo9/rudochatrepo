package com.pgallardo.rudochat.repository

import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.onesignal.OneSignal
import com.pgallardo.rudochat.model.User


class AuthRepository {

    val auth: FirebaseAuth by lazy {
        Firebase.auth
    }
    private val userLiveData: MutableLiveData<User> = MutableLiveData<User>()

    fun loginUser(email: String, password: String): MutableLiveData<User> {
        OneSignal.setSubscription(true)
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                userLiveData.value = getCurrentUser()
            } else {
                userLiveData.value = null
            }

        }
        return userLiveData
    }

    fun registerUser(email: String, name: String, password: String): MutableLiveData<User> {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user = auth.currentUser
                val update = UserProfileChangeRequest.Builder().setDisplayName(name).build()
                user?.updateProfile(update)?.addOnCompleteListener {
                    if (it.isSuccessful) {
                        userLiveData.value = getCurrentUser()
                    }

                }
            } else {
                userLiveData.value = null
            }
        }

        return userLiveData
    }

    fun logOutUser() {
        OneSignal.setSubscription(false)
        auth.signOut()
    }

    fun getCurrentUser():User? {
        if (auth.currentUser != null){
            return User(auth.currentUser?.uid, auth.currentUser?.displayName, auth.currentUser?.email)
        }
        return null
    }

}