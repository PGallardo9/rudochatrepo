package com.pgallardo.rudochat.repository

import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.onesignal.OneSignal
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.model.Message
import com.pgallardo.rudochat.model.User
import org.json.JSONException
import org.json.JSONObject


class DatabaseRepository {

    private val allUsersLiveData = MutableLiveData<MutableList<User>>()
    private val chatsLiveData = MutableLiveData<MutableList<Chat>>()
    private val messagesLiveData = MutableLiveData<Message>()

    val database: FirebaseDatabase by lazy {
        Firebase.database
    }

    val usersRef = database.getReference("users")
    val messagesRef = database.getReference("messages")
    val chatsRef = database.getReference("chats")

    fun createUser(user: User){

        val dbuser = usersRef.child(user.uid!!)
        val notificationId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        dbuser.child("name").setValue(user.name)
        dbuser.child("email").setValue(user.email)
        dbuser.child("notificationId").setValue(notificationId)

    }

    fun sendNotificationId(user: User){
        val notificationId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        usersRef.child(user.uid!!).child("notificationId").setValue(notificationId)
    }


    fun getChats(): MutableLiveData<MutableList<Chat>> {

        val userUid = Firebase.auth.currentUser!!.uid

        chatsRef
            .child(userUid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val chats = snapshot.children
                    val list = mutableListOf<Chat>()

                    chats.forEach {

                        val message: Message? =
                            it.child("lastMessage").getValue(Message::class.java)
                        val map = it.value as HashMap<*, *>
                        list.add(
                            Chat(
                                userUid,
                                map["uid"] as String,
                                map["name"] as String,
                                message
                            )
                        )
                    }

                    chatsLiveData.value = list
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        return chatsLiveData
    }

    fun getAllUsers(): MutableLiveData<MutableList<User>> {

        val userUid = Firebase.auth.currentUser!!.uid

        usersRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val users = dataSnapshot.children
                val list = mutableListOf<User>()

                users.forEach {

                    if (it.key != userUid) {
                        val map = it.value as HashMap<*, *>
                        list.add(User(it.key, map["name"] as String?, map["email"] as String?))
                    }
                }

                allUsersLiveData.value = list

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

        return allUsersLiveData
    }

    fun sendMessage(chat: Chat, message: String) {

        val senderRef = "messages/${chat.senderUid}/${chat.receiverUid}"
        val receiverRef = "messages/${chat.receiverUid}/${chat.senderUid}"

        val userMessageKey = messagesRef.child(chat.senderUid!!).child(chat.receiverUid!!).push().key
        val chatKey = chatsRef.child(chat.senderUid).push().key

        val messageBody = hashMapOf<String, Any>()
        messageBody["message"] = message
        messageBody["senderUid"] = chat.senderUid
        messageBody["timestamp"] = ServerValue.TIMESTAMP

        val chatBody1 = hashMapOf<String, Any>()
        chatBody1["uid"] = chat.receiverUid
        chatBody1["name"] = chat.receiverName!!

        val chatBody2 = hashMapOf<String, Any>()
        chatBody2["uid"] = chat.senderUid
        chatBody2["name"] = Firebase.auth.currentUser!!.displayName!!

        val childUpdate = hashMapOf<String, Any>()
        childUpdate["$senderRef/$userMessageKey"] = messageBody
        childUpdate["$receiverRef/$userMessageKey"] = messageBody

        chatsRef
            .child(chat.senderUid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    snapshot.children.forEach {
                        val map = it.value as HashMap<*, *>
                        if (map["uid"] as String == chat.receiverUid) {
                            childUpdate["chats/${chat.senderUid}/${it.key}/lastMessage"] =
                                messageBody
                            childUpdate["chats/${chat.receiverUid}/${it.key}/lastMessage"] =
                                messageBody
                            database.reference.updateChildren(childUpdate)
                            return
                        }
                    }

                    chatBody1["lastMessage"] = messageBody
                    chatBody2["lastMessage"] = messageBody

                    childUpdate["chats/${chat.senderUid}/$chatKey"] = chatBody1
                    childUpdate["chats/${chat.receiverUid}/$chatKey"] = chatBody2

                    database.reference.updateChildren(childUpdate)

                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        sendNotification(chat, message)

    }

    private fun sendNotification(chat: Chat, message: String){

        usersRef.child(chat.receiverUid!!).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val map = snapshot.value as HashMap<*, *>
                try{
                    OneSignal.postNotification("{'headings': {'en':'${Firebase.auth.currentUser!!.displayName!!}'}," +
                            " 'contents': {'en':'$message'}," +
                            " 'include_player_ids': ['" + map["notificationId"] as String + "']," +
                            " 'android_channel_id': 'fb536ee2-6e63-44ab-8e38-d34bc7e8f73e'," +
                            " 'small_icon': 'ic_message'," +
                            " 'large_icon': 'profile_picture'," +
                            " 'android_accent_color': '#9E27DD'," +
                            " 'android_group': 'group key'," +
                            " 'android_group_message': {'en': '$[notif_count] mensajes nuevos'}," +
                            " 'data': {'senderUid': '${chat.receiverUid}', 'receiverUid': '${chat.senderUid}', 'receiverName': '${Firebase.auth.currentUser!!.displayName!!}'}}", null)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })

    }

    fun getMessages(chat: Chat): MutableLiveData<Message> {

        messagesRef
            .child(chat.senderUid!!)
            .child(chat.receiverUid!!)
            .addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    val message: Message = snapshot.getValue(Message::class.java)!!
                    messagesLiveData.value = message
                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                }

                override fun onChildRemoved(snapshot: DataSnapshot) {

                }

                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        return messagesLiveData
    }

}