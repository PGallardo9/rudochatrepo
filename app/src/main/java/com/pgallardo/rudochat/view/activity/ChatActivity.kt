package com.pgallardo.rudochat.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.onesignal.OneSignal
import com.pgallardo.rudochat.adapter.MessageListAdapter
import com.pgallardo.rudochat.databinding.ActivityChatBinding
import com.pgallardo.rudochat.model.Message
import com.pgallardo.rudochat.viewmodel.ChatViewModel

class ChatActivity : AppCompatActivity() {

    private lateinit var chatViewModel: ChatViewModel
    private lateinit var binding: ActivityChatBinding

    private val messageList = mutableListOf<Message>()
    private lateinit var messageListAdapter: MessageListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        chatViewModel = ViewModelProvider(this).get(ChatViewModel::class.java)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.viewmodel = chatViewModel
        binding.lifecycleOwner = this

        chatViewModel.chat = intent.getParcelableExtra("chat")!!

        val toolbar = binding.toolbar
        toolbar.title = chatViewModel.chat.receiverName
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.messageList.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.messageList.itemAnimator = DefaultItemAnimator()

        messageListAdapter = MessageListAdapter(messageList, chatViewModel)
        binding.messageList.adapter = messageListAdapter

    }

    override fun onStart() {
        super.onStart()

        chatViewModel.getMessages().observe(this, {
            messageList.add(it)
            (binding.messageList.adapter as MessageListAdapter).notifyDataSetChanged()
            binding.messageList.smoothScrollToPosition(messageListAdapter.itemCount)
        })
    }

    override fun onResume() {
        super.onResume()
        OneSignal.setSubscription(false)
    }

    override fun onPause() {
        OneSignal.setSubscription(true)
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
