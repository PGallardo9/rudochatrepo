package com.pgallardo.rudochat.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.pgallardo.rudochat.R
import com.pgallardo.rudochat.adapter.FragmentAdapter
import com.pgallardo.rudochat.databinding.ActivityChatListBinding
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.view.fragment.ChatListFragment
import com.pgallardo.rudochat.view.fragment.ContactListFragment
import com.pgallardo.rudochat.viewmodel.ChatListViewModel

class ChatListActivity : AppCompatActivity() {

    private lateinit var chatListViewModel: ChatListViewModel
    private lateinit var binding: ActivityChatListBinding
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        chatListViewModel = ViewModelProvider(this).get(ChatListViewModel::class.java)
        binding = ActivityChatListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lifecycleOwner = this

        user = intent.getParcelableExtra("user")!!

        val toolbar = binding.toolbar
        toolbar.title = "RudoChat"
        setSupportActionBar(toolbar);

        val fragments = arrayListOf<Fragment>()

        fragments.add(ChatListFragment())
        fragments.add(ContactListFragment())

        binding.pager.adapter = FragmentAdapter(supportFragmentManager, fragments)
        binding.tabLayout.setupWithViewPager(binding.pager)

        binding.tabLayout.getTabAt(0)?.text = "CHATS"
        binding.tabLayout.getTabAt(1)?.text = "CONTACTOS"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.chat_list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.cerrar -> {
                chatListViewModel.onLogOutSelected()
                gotoLoginActivity()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun gotoLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}
