package com.pgallardo.rudochat.view.activity

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.onesignal.OneSignal
import com.pgallardo.rudochat.databinding.ActivityLoginBinding
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.viewmodel.AuthViewModel

class LoginActivity : AppCompatActivity() {

    private lateinit var authViewModel:AuthViewModel
    private lateinit var binding:ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authViewModel = ViewModelProvider(this).get(AuthViewModel::class.java)

        val currentUser = authViewModel.getCurrentUser()

        if (currentUser != null) {
            gotoChatListActivity(currentUser)
        } else {
            binding = ActivityLoginBinding.inflate(layoutInflater)
            setContentView(binding.root)
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

            binding.viewmodel = authViewModel
            binding.lifecycleOwner = this

            initFieldObservers()
            initButtonObservers()
        }
    }

    private fun gotoChatListActivity(currentUser: User) {
        val intent = Intent(this, ChatListActivity::class.java).apply {
            putExtra("user", currentUser)
        }

        startActivity(intent)
        finish()
    }

    private fun initFieldObservers() {

        authViewModel.email.observe(this){clearErrors()}

        authViewModel.password.observe(this){clearErrors()}

        authViewModel.emailError.observe(this, {
            binding.editEmail.error = it
            binding.editEmail.requestFocus()
        })

        authViewModel.passwordError.observe(this, {
            binding.editPassword.error = it
            binding.editPassword.requestFocus()
        })
    }

    private fun initButtonObservers(){

        authViewModel.loginButton.observe(this, {
            authViewModel.userLiveData.observe(this, {
                binding.loadingButton.hideLoading()
                if(it != null) {
                    authViewModel.sendNotificationId(it)
                    gotoChatListActivity(it)
                } else {
                    binding.editEmail.error = "Usuario o contraseña incorrectos"
                    binding.editEmail.requestFocus()
                }
            })
        })

        authViewModel.createAccountButton.observe(this, {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        })

    }

    override fun onResume() {
        super.onResume()
        clearErrors()
    }

    private fun clearErrors(){
        binding.editEmail.error = null
        binding.editPassword.error = null
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}
