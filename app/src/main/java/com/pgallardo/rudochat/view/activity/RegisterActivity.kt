package com.pgallardo.rudochat.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.pgallardo.rudochat.databinding.ActivityRegisterBinding
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.viewmodel.AuthViewModel

class RegisterActivity : AppCompatActivity() {

    private lateinit var authViewModel:AuthViewModel
    private lateinit var binding:ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authViewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        binding.viewmodel = authViewModel
        binding.lifecycleOwner = this

        initFieldObservers()
        initButtonObservers()
    }

    private fun gotoChatListActivity(currentUser: User) {
        val intent = Intent(this, ChatListActivity::class.java).apply {
            putExtra("user", currentUser)
        }

        startActivity(intent)
        finish()
    }

    private fun initFieldObservers() {

        authViewModel.email.observe(this, {clearErrors()})

        authViewModel.name.observe(this, {clearErrors()})

        authViewModel.password.observe(this, {clearErrors()})

        authViewModel.emailError.observe(this, {
            binding.editEmail.error = it
            binding.editEmail.requestFocus()
        })

        authViewModel.nameError.observe(this, {
            binding.editName.error = it
            binding.editName.requestFocus()
        })

        authViewModel.passwordError.observe(this, {
            binding.editPassword.error = it
            binding.editPassword.requestFocus()
        })
    }

    private fun initButtonObservers(){

        authViewModel.registerButton.observe(this, {
            authViewModel.userLiveData.observe(this, {
                binding.loadingButton.hideLoading()
                if(it != null) {
                    authViewModel.createUserInDB(it)
                    gotoChatListActivity(it)
                } else {
                    binding.editEmail.error = "Error"
                    binding.editEmail.requestFocus()
                }
            })
        })

        authViewModel.clearButton.observe(this, {
            onBackPressed()
        })

    }

    override fun onResume() {
        super.onResume()
        clearErrors()
    }

    private fun clearErrors(){
        binding.editEmail.error = null
        binding.editName.error = null
        binding.editPassword.error = null
    }
}
