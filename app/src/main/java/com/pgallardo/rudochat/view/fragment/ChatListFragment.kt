package com.pgallardo.rudochat.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.pgallardo.rudochat.adapter.ChatListAdapter
import com.pgallardo.rudochat.databinding.ChatListFragmentBinding
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.view.activity.ChatActivity
import com.pgallardo.rudochat.viewmodel.ChatListViewModel

class ChatListFragment : Fragment() {

    private lateinit var viewModel: ChatListViewModel
    private lateinit var binding: ChatListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(ChatListViewModel::class.java)

        binding = ChatListFragmentBinding.inflate(layoutInflater)
        val view = binding.root

        binding.recyclerView.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerView.itemAnimator = DefaultItemAnimator()


        viewModel.clickedChat.observe(viewLifecycleOwner, {
            gotoChatActivity(it)
        })

        return view
    }

    private fun gotoChatActivity(chat: Chat) {
        val intent = Intent(context, ChatActivity::class.java).apply {
            putExtra("chat", chat)
        }

        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        binding.shimmerViewContainer.startShimmerAnimation()

        viewModel.getChats().observe(viewLifecycleOwner, { chatList ->
            chatList.sortWith(compareByDescending{ it.lastMessage?.timestamp})
            binding.recyclerView.adapter = ChatListAdapter(chatList, viewModel)
            binding.shimmerViewContainer.stopShimmerAnimation()
            binding.shimmerViewContainer.visibility = View.GONE
        })
    }

    override fun onPause() {
        binding.shimmerViewContainer.stopShimmerAnimation()
        super.onPause()
    }

}