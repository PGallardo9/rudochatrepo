package com.pgallardo.rudochat.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.pgallardo.rudochat.adapter.ContactListAdapter
import com.pgallardo.rudochat.databinding.ContactListFragmentBinding
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.view.activity.ChatActivity
import com.pgallardo.rudochat.viewmodel.ChatListViewModel

class ContactListFragment : Fragment() {

    private lateinit var viewModel: ChatListViewModel
    private lateinit var binding: ContactListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(ChatListViewModel::class.java)

        binding = ContactListFragmentBinding.inflate(layoutInflater)
        val view = binding.root

        binding.recyclerView.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerView.itemAnimator = DefaultItemAnimator()

        viewModel.getContacts().observe(viewLifecycleOwner, {
            binding.recyclerView.adapter = ContactListAdapter(it, viewModel)
            binding.shimmerViewContainer.stopShimmerAnimation()
            binding.shimmerViewContainer.visibility = View.GONE
        })

        viewModel.clickedUser.observe(viewLifecycleOwner, {
            gotoChatActivity(it)
        })

        return view

    }

    private fun gotoChatActivity(chat: Chat) {
        val intent = Intent(context, ChatActivity::class.java).apply {
            putExtra("chat", chat)
        }

        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        binding.shimmerViewContainer.startShimmerAnimation()
    }

    override fun onPause() {
        binding.shimmerViewContainer.stopShimmerAnimation()
        super.onPause()
    }

}
