package com.pgallardo.rudochat.viewmodel

import android.util.Patterns
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kusu.loadingbutton.LoadingButton
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.repository.AuthRepository
import com.pgallardo.rudochat.repository.DatabaseRepository


class AuthViewModel : ViewModel(){

    private val authRepo = AuthRepository()
    private val dbRepo = DatabaseRepository()

    val email: MutableLiveData<String> = MutableLiveData<String>()
    val password: MutableLiveData<String> = MutableLiveData<String>()
    val name: MutableLiveData<String> = MutableLiveData<String>()

    val emailError: MutableLiveData<String> = MutableLiveData<String>()
    val passwordError: MutableLiveData<String> = MutableLiveData<String>()
    val nameError: MutableLiveData<String> = MutableLiveData<String>()

    var userLiveData: MutableLiveData<User> = MutableLiveData()

    val loginButton: MutableLiveData<View> = MutableLiveData<View>()
    val createAccountButton: MutableLiveData<View> = MutableLiveData<View>()
    val registerButton: MutableLiveData<View> = MutableLiveData<View>()
    val clearButton: MutableLiveData<View> = MutableLiveData<View>()

    fun onLoginButtonClick(view: View){

        val button = view as LoadingButton

        when{
            !isEmailValid() -> emailError.value = "Email inválido"
            !isPasswordValid() -> passwordError.value = "Contraseña de al menos 6 caracteres"
            else -> {
                button.showLoading()
                userLiveData = authRepo.loginUser(email.value!!, password.value!!)
                loginButton.value = view
            }
        }

    }

    fun onRegisterButtonClick(view: View){

        val button = view as LoadingButton

        when{
            !isEmailValid() -> emailError.value = "Email inválido"
            !isNameValid() -> nameError.value = "Nombre vacío"
            !isPasswordValid() -> passwordError.value = "Contraseña de al menos 6 caracteres"
            else -> {
                button.showLoading()
                userLiveData = authRepo.registerUser(email.value!!, name.value!!, password.value!!)
                registerButton.value = view
            }
        }

    }

    fun onCreateAccountButtonClick(view: View){
        createAccountButton.value = view
    }

    fun onClearButtonClick(view: View){
        clearButton.value = view
    }

    fun createUserInDB(user: User) = dbRepo.createUser(user)

    fun getCurrentUser(): User? = authRepo.getCurrentUser()

    fun sendNotificationId(user: User) = dbRepo.sendNotificationId(user)


    private fun isEmailValid(): Boolean {
        val value = email.value
        return if (!email.value.isNullOrEmpty()) Patterns.EMAIL_ADDRESS.matcher(value).matches() else false
    }

    private fun isNameValid(): Boolean = !name.value.isNullOrEmpty()

    private fun isPasswordValid(): Boolean {
        val value = password.value
        return if (!value.isNullOrEmpty()) value.length > 5 else false
    }

}