package com.pgallardo.rudochat.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.repository.AuthRepository
import com.pgallardo.rudochat.repository.DatabaseRepository

class ChatListViewModel : ViewModel(){

    val authRepo = AuthRepository()
    val dbRepo = DatabaseRepository()

    val clickedChat = MutableLiveData<Chat>()
    val clickedUser = MutableLiveData<Chat>()

    val currentUser = authRepo.getCurrentUser()

    fun onLogOutSelected() = authRepo.logOutUser()

    fun onChatClicked(chat: Chat){
        clickedChat.value = chat
    }

    fun onUserClicked(user: User){
        clickedUser.value = Chat(authRepo.auth.uid, user.uid, user.name, null)
    }

    fun getChats(): MutableLiveData<MutableList<Chat>> {
        return dbRepo.getChats()
    }

    fun getContacts(): MutableLiveData<MutableList<User>> {
        return dbRepo.getAllUsers()
    }

}