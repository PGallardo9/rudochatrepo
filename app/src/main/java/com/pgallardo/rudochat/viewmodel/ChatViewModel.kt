package com.pgallardo.rudochat.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pgallardo.rudochat.model.Chat
import com.pgallardo.rudochat.model.Message
import com.pgallardo.rudochat.model.User
import com.pgallardo.rudochat.repository.AuthRepository
import com.pgallardo.rudochat.repository.DatabaseRepository

class ChatViewModel : ViewModel() {

    private val authRepo = AuthRepository()
    val dbRepo = DatabaseRepository()

    val message: MutableLiveData<String> = MutableLiveData<String>()
    lateinit var chat: Chat

    fun getCurrentUser(): User? = authRepo.getCurrentUser()

    fun onSendButtonClick(view: View) {

        if(!message.value.isNullOrEmpty()) {
            dbRepo.sendMessage(chat, message.value!!)
            message.value = ""
        }
    }

    fun getMessages(): MutableLiveData<Message>{
        return dbRepo.getMessages(chat)
    }

}